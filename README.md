# README #

The Tablet Crasher Tester is a Chrome extension. It checks if your website is tablet compatible (i.e. if it doesn't crash on a tablet). It's useful because it not only gives you information about your current website, it also advises you what you could do better (technically).

### Installation ###
* blabla
* blabla

### Usage ###
* blabla
* blabla

### Contact ###
Bilal Hussain / bilalh@fabrique.nl

Martijn Gorree / martijng@fabrique.nl